package com.example.a7.springdatajpa.repository;

        import com.example.a7.springdatajpa.model.MemberType;
        import org.springframework.data.jpa.repository.JpaRepository;
        import org.springframework.stereotype.Repository;

@Repository
public interface MemberTypeRepository extends JpaRepository<MemberType, Long> {

}
