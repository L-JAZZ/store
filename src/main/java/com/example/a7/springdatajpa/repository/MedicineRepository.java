package com.example.a7.springdatajpa.repository;

import com.example.a7.springdatajpa.model.Medicine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicineRepository extends JpaRepository<Medicine, Long> {
    @Query("select b from Medicine b where b.quantity > 0")
    List<Medicine>findAvailableBook();

    List<Medicine> getBookByTitle(String title);
}

