package com.example.a7.springdatajpa.exceptions;

import org.springframework.stereotype.Service;

@Service
public class ExceptionService {

    public int testException() {
        throw new NullPointerException("Some exception");
    }
}
