package com.example.a7.springdatajpa.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
@Getter
@Setter
@Entity
public class MemberType implements GrantedAuthority {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long membertype_id;
    private String name;

    @Override
    public String toString() {
        return  "\n"+ "MemberType{" +
                "id=" + membertype_id +
                ", type='" + name + '\'' +
                '}';
    }

    public MemberType(){

    }


    @Override
    public String getAuthority() {
        return name;
    }
}
