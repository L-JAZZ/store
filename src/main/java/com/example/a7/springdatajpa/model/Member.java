package com.example.a7.springdatajpa.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;


@Getter
@Setter
@Entity
public class Member implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long member_id;
    private String phoneNum;
    private String username;
    private String password;


    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "issuedDrug",
            joinColumns = {@JoinColumn(name = "member_id", referencedColumnName = "member_id")},
            inverseJoinColumns = {@JoinColumn(name="medicine_id", referencedColumnName = "medicine_id")}
    )
    private List<Medicine> medicine;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "member_types",
            joinColumns = {@JoinColumn(name = "member_id", referencedColumnName = "member_id")},
            inverseJoinColumns = {@JoinColumn(name = "membertype_id", referencedColumnName = "membertype_id")}
    )
    private List<MemberType> types;



    public List<MemberType> getTypes() {
        return types;
    }

    public void setTypes(List<MemberType> types) {
        this.types = types;
    }

    public List<Medicine> getMedicines() {
        return medicine;
    }

    public void setMedicines(List<Medicine> medicines) {
        this.medicine = medicines;
    }

    @Override
    public String toString() {
        return  "\n"+ "Member{" +
                "id=" + member_id +
                ", phoneNum='" + phoneNum + '\'' +
                ", Username='" + username + '\'' +
                ", Password='" + password + '\'' +
                '}';
    }

    public Member(){

    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return types;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
