package com.example.a7.springdatajpa.model;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;


@Getter
@Setter

@Entity
public class Doctor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String surname;

    @OneToMany(mappedBy = "doctor", fetch = FetchType.LAZY)
    private List<Medicine> medicines;

    public List<Medicine> getMedicines() {
        return medicines;
    }

    public void addMedicine(Medicine medicine){
        medicines.add(medicine);
    }

    public void setMedicines(List<Medicine> medicines) {
        this.medicines = medicines;
    }

    @Override
    public String toString() {
        return "\n"+"Doctor{" + "\n"+
                "id=" + id + "\n"+
                ", name='" + name + '\'' + "\n"+
                ", surname='" + surname + '\'' + "\n"+
                ", books = " + medicines + "\n"+
                '}';
    }

    public Doctor(){

    }
}
