package com.example.a7.springdatajpa.service.impl;

import com.example.a7.springdatajpa.model.Member;
import com.example.a7.springdatajpa.model.MemberType;
import com.example.a7.springdatajpa.repository.MemberRepository;
import com.example.a7.springdatajpa.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class UserServiceImpl implements UserService, UserDetailsService {
    @Autowired
    MemberRepository memberRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;



    @Override
    public List<Member> getAllMembers() {
        return memberRepository.findAll();
    }

    @Override
    public void createMember(Member member) {
        member.setPassword(passwordEncoder.encode(member.getPassword()));
        memberRepository.saveAndFlush(member);
    }

    @Override
    public void updateMember(Long id, Member member) {
        Member memberDb = memberRepository.findById((long) id).orElse(null);
        if (memberDb!=null){
            memberRepository.saveAndFlush(memberDb);
        }
    }

    @Override
    public void deleteMember(int id) {
        Member member = memberRepository.findById((long) id).get();
        memberRepository.delete(member);
    }

    @Override
    public Member findMemberById(int id) {
        return memberRepository.findById((long) id).get();
    }

    @Override
    public void updateMemberType(MemberType memberType, int id) {
        Member member = memberRepository.findById((long) id).get();
        memberRepository.saveAndFlush(member);
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Member member = memberRepository.findByUsername(s);
        if(member == null){
            throw  new UsernameNotFoundException("User: " + s + " not found");
        }
        return member;
    }
}
