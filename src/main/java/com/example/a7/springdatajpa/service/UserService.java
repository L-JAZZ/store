package com.example.a7.springdatajpa.service;

import com.example.a7.springdatajpa.model.Member;
import com.example.a7.springdatajpa.model.MemberType;

import java.util.List;

public interface UserService {
    List<Member> getAllMembers();
    void createMember(Member member);
    void updateMember(Long id, Member member);
    void deleteMember(int id);
    Member findMemberById(int id);
    //
    void updateMemberType(MemberType memberType, int id);
}
