package com.example.a7.springdatajpa.config;

import com.example.a7.springdatajpa.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserServiceImpl userService;

    @Bean
    public BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests()

                .antMatchers("/api/members/create/**").hasAuthority("admin")
                .antMatchers("/api/members/").hasAnyAuthority()
                .antMatchers("/api/doctors/").hasAuthority("admin")
                .antMatchers("/api/doctors/all").permitAll()
                .antMatchers("/api/doctors/addAuthor").hasAuthority("admin")
                .antMatchers("/api/members/all/").hasAnyAuthority()
                .antMatchers("/api/medicine/all/").permitAll()
                .antMatchers("/auth/**").permitAll()
                .antMatchers("/api/controller/**").hasAuthority("admin")
                /*.anyRequest().authenticated()*/
                .and().formLogin()
                .and()
                .addFilter(new JwtTokenGeneratorFilter(authenticationManager()))

                // Add a filter to validate the tokens with every request
                .addFilterAfter(new JwtTokenAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);

    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService).passwordEncoder(passwordEncoder());
    }
}
