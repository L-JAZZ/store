package com.example.a7.springdatajpa.controllers;

import com.example.a7.springdatajpa.exceptions.ExceptionService;
import com.example.a7.springdatajpa.exceptions.GlobalExceptionHandler;
import com.example.a7.springdatajpa.model.Doctor;
import com.example.a7.springdatajpa.model.Medicine;
import com.example.a7.springdatajpa.repository.DoctorRepository;
import com.example.a7.springdatajpa.repository.MedicineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/doctors")
public class DoctorController {

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private MedicineRepository medicineRepository;

    //all exception
    @Autowired
    private GlobalExceptionHandler exceptionService;

    @GetMapping
    public ResponseEntity<?> testException() {
        return ResponseEntity.ok(exceptionService.handleAnyException(new Exception()));
    }

    @GetMapping("/addDoctor")
    public void addDoctor(String name, String surname){
        Doctor doctor = new Doctor();
        doctor.setName(name);
        doctor.setSurname(surname);
        doctorRepository.save(doctor);
    }

    @GetMapping("/all")
    public String getDoctorList(){
        return  doctorRepository.findAll().toString();
    }

    @GetMapping("/{id}")
    public Doctor getDoctorByID(@PathVariable("id") Long id){
        return doctorRepository.findById(id).get();
    }

    @PutMapping("/addMedicine/")
    public void addMedicineToDoctor(Long doctorid,Long medid){
        Doctor doctor = doctorRepository.findById(doctorid).get();
        Medicine medicine = medicineRepository.findById(medid).get();
        medicine.setDoctor(doctor);
        medicineRepository.saveAndFlush(medicine);
        doctorRepository.saveAndFlush(doctor);
    }

    @GetMapping("/find/")
    public List<Doctor> getDoctorByName(@RequestParam("name") String name){
        return doctorRepository.findAllByNameContains(name);
    }

    public DoctorController(DoctorRepository doctorRepository){
        this.doctorRepository = doctorRepository;
    }



    @DeleteMapping("/{id}")
    public void removeDoctor(@PathVariable("id") Long id){
        Doctor doctor = doctorRepository.findById(id).get();
        doctorRepository.delete(doctor);
    }

    @GetMapping("/updateDoctor")
    public void updateDoctor(Long id, String name, String surname){
        Doctor doctor = doctorRepository.findById(id).get();
        doctor.setName(name);
        doctor.setSurname(surname);
        doctorRepository.saveAndFlush(doctor);
    }

    @PatchMapping("/{id}")
    public Doctor updateDoctorName(@PathVariable Long id,
                                   @RequestBody String name){
        Doctor doctor = doctorRepository.findById(id).get();
        doctor.setName(name);
        return doctorRepository.saveAndFlush(doctor);
    }

}
