package com.example.a7.springdatajpa.controllers;
/*
{
        "quantity": "5",
        "title": "How to be a millionaire",
        "year": "2016",
}
*/

import com.example.a7.springdatajpa.exceptions.ExceptionService;
import com.example.a7.springdatajpa.exceptions.GlobalExceptionHandler;
import com.example.a7.springdatajpa.model.Medicine;
import com.example.a7.springdatajpa.repository.MedicineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/medicine")
public class MedicineController {
    @Autowired
    private MedicineRepository medicineRepository;

    @Autowired
    private GlobalExceptionHandler exceptionService;

    @GetMapping
    public ResponseEntity<?> testException() {
        return ResponseEntity.ok(exceptionService.handleAnyException(new Exception()));
    }

    @PostMapping("")
    public void addMedicine(@RequestBody Medicine medicine){
        medicineRepository.save(medicine);
    }

    @GetMapping("/find/")
    public List<Medicine> getMedicineByTitle(@RequestParam("title") String title){
        return  medicineRepository.getBookByTitle(title);
    }

    @GetMapping("/available")
    public String availableMedicineList(){
        return  medicineRepository.findAvailableBook().toString();
    }

    @GetMapping("/all")
    public String getAllMedicine(){
        return medicineRepository.findAll().toString();
    }

    @PutMapping("/rent/{id}")
    public void rentMedicine(@PathVariable Long id){
        Medicine medicine = medicineRepository.findById(id).get();
        medicine.buyMedicine();
        medicineRepository.saveAndFlush(medicine);
    }

/*    @PutMapping("/take/{id}")
    public void takeMedicineBack(@PathVariable Long id) {
        Medicine medicine = medicineRepository.findById(id).get();
        medicine.takeBookBack();
        medicineRepository.saveAndFlush(medicine);
    }*/

    @DeleteMapping("{id}")
    public void deleteMedicine(@PathVariable Long id){
        Medicine medicine = medicineRepository.findById(id).get();
        medicineRepository.delete(medicine);
    }

    public MedicineController(MedicineRepository medicineRepository){
        this.medicineRepository = medicineRepository;
    }
}
