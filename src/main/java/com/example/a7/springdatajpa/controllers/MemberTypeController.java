package com.example.a7.springdatajpa.controllers;

import com.example.a7.springdatajpa.exceptions.ExceptionService;
import com.example.a7.springdatajpa.exceptions.GlobalExceptionHandler;
import com.example.a7.springdatajpa.model.Member;
import com.example.a7.springdatajpa.model.MemberType;
import com.example.a7.springdatajpa.repository.MemberTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/controller")
public class MemberTypeController {
    @Autowired
    MemberTypeRepository memberTypeRepository;


    @GetMapping("")
    public void addMemberType(Long typeID,String name){
        MemberType memberType = new MemberType();
        memberType.setMembertype_id(typeID);
        memberType.setName(name);
        memberTypeRepository.save(memberType);
    }

}
